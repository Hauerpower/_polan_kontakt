<section id="home-slider" class="slider slick-slider slider--main slider--full-height">
	
	<div class="slide" style="background-image: url('https://picsum.photos/seed/picsum/1920/1080')">
		
		<div class="slide-wrap">
			<div class="slide-inner">
			
				<div class="grid-container">
					<h2>Hello World</h2>
					<h3>Full height slider</h3>
				</div>
				
			</div>
		</div>
		
	</div>
	
	<div class="slide" style="background-image: url('https://picsum.photos/seed/picsum/1920/1080')">
		
		<div class="slide-wrap">
			<div class="slide-inner">
			
				<div class="grid-container">
					<h2>Slide 2</h2>
					<h3>Full height slider</h3>
					<h3>Full height slider</h3>
					<h3>Full height slider</h3>
				</div>
				
			</div>
		</div>
		
	</div>
	
</section>